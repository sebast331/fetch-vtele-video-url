# README #

Retrieve the best MP4 video url for any video page given.

1. Go to http://vtele.ca
1. Browse the site and open a page containing a video
1. Copy the URL and use it in the script as a parameter

### Table of contents ###
* [Python Version](#markdown-header-python-version)
* [Usage](#markdown-header-usage)
* [Play video in VLC](#markdown-header-play-video-in-vlc)

### Python Version ###
This script runs on Python2.7

### Usage ###

```
python2.7 get_video_URL.py <video_page>
```

Return the example video. Page used is http://auto.vtele.ca/videos/rpm/ultra-mousse-et-lave-cire-disparu-la-scion-xd_84635_84605.php

```
#!bash
$ python2.7 get_video_url.py
http://uds.ak.o.brightcove.com/618566855001/618566855001_4486918741001_4486848066001.mp4
```

Return the video file using a specific page

```
#!bash
$ python2.7 get_video_url.py http://vtele.ca/videos/ces-gars-la/top-v-l-inimitable-massimo_83088.php
http://uds.ak.o.brightcove.com/618566855001/618566855001_4305946594001_4305763462001.mp4
```

### Play video in VLC ###
It is possible to directly play the video in VLC using this line

```
#!bash
$ vlc $(python2.7 get_video_url.py http://vtele.ca/videos/ces-gars-la/top-v-l-inimitable-massimo_83088.php)
```